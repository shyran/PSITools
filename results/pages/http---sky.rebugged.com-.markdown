---
layout: result
title: http://sky.rebugged.com/
MinimizeRenderBlockingResources: true
PrioritizeVisibleContent: true
SizeTapTargetsAppropriately: true
UseLegibleFontSizes: true
---
## [Springle Chat](http://sky.rebugged.com/)

**Check against [PageSpeed Insights live](https://developers.google.com/speed/pagespeed/insights/?url=http://sky.rebugged.com/)**

**Score**: [72](https://developers.google.com/speed/pagespeed/insights/?url=http://sky.rebugged.com/)


<img src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAIUAUADASIAAhEBAxEB/8QAHAABAAMBAQEBAQAAAAAAAAAAAAIDBQQBBgcI/8QAOBABAAECBAIJBAECBQUBAAAAAAECAwQFERIhMQYTFkFSVZGU0RQiUWFxFTIjM4GhsSQ0QmPwgv/EABcBAQEBAQAAAAAAAAAAAAAAAAACAQP/xAAmEQEAAgEDAwIHAAAAAAAAAAAAAQIRAxMxBBIhM1FSYXKBkcHw/9oADAMBAAIRAxEAPwD8VwVj6nG4fD7tvXXaLe7TXTdVEa/7voa+iVcYu7R9Zapw1NNcxcu/bVTVFMztriNdOMaTpMvmbN+bN63dtXIpuW6orpq1jhMTrE+rQwWf4/BWeqw2Lii31lV3btoqjdVExPOJ56zw5cQdebdHb2XYWL9y7RFNMU01xXOk9ZMUztp01151fjhS7sy6GYnDYuu3YxNmq3RG6qq/OyqimOdUxG6Nsa0zrE6/fHDnph387x2IsV2b+M6y3XEaxXFM8tNJ5c+EcefP8yvp6TZpTppj54VTVxponjMzM93GJmZ4cuX4gFma9HcZlmBpxWJuYaY1imq3Rcma6Jn8xpp+OUzzYzsxWcYzF2Js4nFzctzMTMVTHHTTTj/pDh30+Kn1BIR30+Kn1N9Pip9QSEd9Pip9TfT4qfUEhHfT4qfU30+Kn1BIR30+Kn1N9Pip9QSEd9Pip9TfT4qfUEhHfT4qfU30+Kn1BIR30+Kn1N9Pip9QSEd9Pip9TfT4qfUEhHfT4qfU30+Kn1BIR30+Kn1N9Pip9QSEd9Pip9TfT4qfUEhHfT4qfU30+Kn1BIR30+Kn1N9Pip9Qb3RXo5e6RYrqLOIt2J4/dXTM8o17nZmHQ7EYPHZhhqsbYrqwdnrpqimYiv7ddI/Dh6KdIp6P46MRTZoxERr9s3NvONGvmXTa3jfrcRThKLeKxNPVTT1mtMU7dNde+f0npe6epvven2+PqzH34z8nXqp04po7PPnv/X9DJxfRrH2bl+3h6YxlzC0RVios01f4EzGsRM1RG7WImdadYnSUquiedxa3f0+9NURNVVuNN1NMRTMTPHv3RpHP9cYa2E6VZfZyHqYsXKr+ErtzhYvYqK65nS5xmdvG3TujSnhPHmoo6aUUTdn6KKt2LjFaddHdNudvL/18/wB/pTkz6Oi+af0+cTdw1y3NfVTYtzEa3ormY1jjwiNNeOjksZTequY2MZct4GjBRHX134qnbVM6U06UxMzMz+O6Jl9jmvSzAYG7YuYC/Rjprt0Wq6aa6YimimnSap1ojSuvfXrE7tNsazOuj5WvO7OIxGafX272Jw2P2TVrfppu01Uf2Vbop2zw1iY26aTw00BZmnRjM8DjbtmjDXL9FNrrqblMREV0bYmaojXu15c+/Rm5pgL+V5hfwWLimL9mYiqKaoqjjETzj+X09rpthqYxE15bRVcudbNFzrad1G+3TRMazTPD7YmdNJnSI1011+ZznH28xzO/i6aYtddMVTRNcVaTtiJ48OGsA/t3sxkXkmV+0t/B2YyHyTK/aW/hsAMfsxkPkmV+0t/B2YyHyTK/aW/hsAMfsxkPkmV+0t/B2YyHyTK/aW/hsAMfsxkPkmV+0t/B2YyHyTK/aW/hsAMfsxkPkmV+0t/B2YyHyTK/aW/hsAMfsxkPkmV+0t/B2YyHyTK/aW/h35ji4wWFqvzbru6TTTFFHOZmYiNPVm9p8ti1FdVd2nWJnTq5mY001/3mI/c8gT7MZD5JlftLfwdmMh8kyv2lv4W3c8wNrE04euu51tVVNMRFuZ41co/+7ol5TnuAmcRpdq0sTpXVt4a66cJ7+OnrAK+zGQ+SZX7S38OTFZV0XwmIos4nKcst1V07omcHRpprpz2r56T5fvppib07ojT7NJmZnSI0meff/rCvEZvk+LpqrxFuq5atU01767c6cZnSIjn3a/jjAOevA9EqJpirLcpiJmqmKvo6dNadNY128/uh59J0OjX/AKLJtYjWY+lo1iP39vBOnMMhoptUfT1U0W9aqIqtVcNdNZiJnWeXP8w9quZFbroiMHTG61F6qqadNtNNMzEzMzzjT1mAceL7FYTFWLGIwGVUVXrfW01Tgqdu3XTjO3SNeP8ApEzyVYXEdBcXfs2cLg8qvV3v7NmAjTTSZ1mdmkREUzr+HbjZ6N3sbRXjMFRcu2qKKKbldmZjb/dERx4xGuvr/CjAVdGLd63dw+XTavxXERXFqrdRMRtiZq17qeH6iQdM4HojFUUzgMniZ054Sjv/APy8jA9EZprq/p+URRRt3VTg6YiN3KNZp58OSnrOjtq7VXXh7lmbdelVOydsTRNUfnjE/wDGjswmIyLGTRg7GH303dulE2p2zzmJnX+JnWQc9OF6H1V1UxgMomaY11jB0TE8+X28eX/H5grwvQ+ia4qy/KYmmrb/ANnROvDXh9vGNO9O9ishqnScLNdU1RTVTFE60fdPGY14aTM/v0Wzdyam5eizhaaq7V2bdyaYmNs6VTOkxziNvdy4cmxE2nEC21kHR27Xtt5NllU7Ir1+jo00nl/4/pb2YyHyTK/aW/hTZzfLbF2JtW79uiLe2Z6uYiIp4xGn8S7ZzfDzdu2bUXK7tuImadukcZpiOP8ANUek/hVtK9cZjlmVHZjIfJMr9pb+DsxkPkmV+0t/CVefYOmJ062uruii3M6z+v8A7uT/AKxZnD0X7du9XbnXWYp0mNKqaZ0iefGqOX7Vs6nsZhV2YyHyTK/aW/g7MZD5JlftLfwnGfYKZj/P0nv6qdIj8z+mhhb9GJw9u9a3bK43RujSdP4TbTvSM2jBlmdmMh8kyv2lv4OzGQ+SZX7S38NgQ1j9mMh8kyv2lv4OzGQ+SZX7S38NgBj9mMi8lyv2lv4OzGQ+SZX7S38NgBj9mMi8lyv2lv4OzGQ+SZX7S38NgBj9mMh8kyv2lv4OzGQ+SZX7S38NgAAAAAAAAAAB5VTFX90RPfxUzg8NM6zhrEzy1m3T8LwFU4axN3rJs2us4Tv2Rrw5cXn0uH2VU9RZ21c46uNJ468eH5XAOW1l+DtU26aMLYiKKYpp/wAONYiP3p+59VkYaxEzMWLUTMRE/ZHHTl/zK4BT9Jh9Yn6ezrGnHq47uXc9qw1irXdYtTrTtnWiOMfj+OM8P2tAVTh7MzGtm1OkxMa0Rzjk8owuHopimixZpiOURRERH+y4BTOFw887FnnNX+XHOddZ5c+M+p9Jh+6xaj+KIhcAojCYaKt0YexFWuuvV06/8IXcBhLtNVNeHtTFU7p0jbrPHnMaa859XUNiZicwM2cjyyeeDtz38Zq/Gn5dVzBYa5ZrtV2Lc264imuNNN0RpwmY4zyj0dAqdS88zJhk3ejuVXadteCo011+2uuJ7vxP6j0WzkuXTEROEo0iZmPuq4TPPvaI3e1Pin8yzEM6Mky2J1jB24n9VVfLtw9i1hrNNqxRFFunlTHcsE2va3i0zLcACQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//2Q=='>

### Request information

*  Number of Resources: 10
*  Number of Hosts: 1
*  Number of JS Resources: 2
*  Number of CSS Resources: 1
*  Nubmer of Static Resources: 7
*  Total Request Bytes: 572

### Response data

*  HTML Response Bytes: 4385
*  Text Response Bytes: 0
*  CSS Response Bytes: 13473
*  Image Response Bytes: 25190
*  JS Response Bytes: 106015
*  Flash Response Bytes: 0
*  Other Response Bytes: 575425

### Performance issues

*  Eliminate render-blocking JavaScript and CSS in above-the-fold content
*  Prioritize visible content

### Mobile UX issues

*  Size tap targets appropriately
*  Use legible font sizes
