---
layout: result
title: http://epos.menu/~in2vietn/menu/ezPOS/login.php
UseLegibleFontSizes: true
---
## [404 Not Found](http://epos.menu/~in2vietn/menu/ezPOS/login.php)

**Check against [PageSpeed Insights live](https://developers.google.com/speed/pagespeed/insights/?url=http://epos.menu/~in2vietn/menu/ezPOS/login.php)**

**Score**: [100](https://developers.google.com/speed/pagespeed/insights/?url=http://epos.menu/~in2vietn/menu/ezPOS/login.php)


<img src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAIUAUADASIAAhEBAxEB/8QAHAABAAIDAQEBAAAAAAAAAAAAAAIDBAUGAQcI/8QAOBABAAIBAwIEBAQEBAcBAAAAAAECAwQRIQUSBhQxQRNRU5MiYXGRBxUygRYjQmIzNENSobHh8P/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD9C9X8Z9J6R4v6P4c118uPX9Vpe2mt2f5czX/TNt+Jn2459EugeMuida6V0nXYdbi08dUr3aTBqslcebLzttFJneZ3+W7ReOvAebxP4h8/XV49NGPpeTTae8RPxMOqjUYs2HNHttW2Ln3nfb3lx+n/AIQdXxdAr0u2u6Vk8507TdP1eoyYb2yaX4OS9+/TTxzPfvtO21oi3PoD6d03xh0bqek61qOn6uupx9Iy5cOrjHzNb4672iI9/eIn0naWD4T8dafxFprauOkdZ6Z07y0aumt6lgphw3xzETExbvn2nfnbhj9D8I6zpvT/ABjo5zaSadY1mp1WnyUrMTT41Nu28bf6Z949YaLS/wAIum6DwDh6T0vBoNJ1j4OjnVaj4dr4tXlwXpkmuSszvOO9qTvHE7T/AGB9F03Wul6qmmvpepaLNTUxe2C2PPS0ZYr/AFTXafxbe+3opp4j6JfPp8NOsdNtm1EUnDjjVUm2TvjevbG/O8cxt6w+cU/h117B1XD1zSarouHqdtbqtRl0tcWSNLjpn0+PDMUmPxTaPhVvMzEd02n09WN03+Euu0vR8unyanp19Z5fomHFm7Lb0nRXrbJtO28Rbtnbb+4Poubxd0enibRdBx6qmfqOq+NHZgtW/wAGcVYtaMm071na0bRMcugfK/C38Oup9H8VdF1mXP0u+g6VbqHZlx0vGp1FdTbvick7bTNZ4nnn199n1QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGPl12lw6mmny6jDTPeN647XiLWjfbiP1ZG8Nf1TpWn6nEV1W80ivb28TE/irbmJ/OkNbXwro40GTSTn1FqXyXyRa14m0WtSa777fKQdFuhOWkWis3rFrcxEzzLl48G6eNVfJXXamuO+OuO1a9sWmInfm22/rt+cbbRwX8E6G1ZjzGeP8utN47faNt/T32iPltGwOntnxVtNbZaRaOJibRv6TP/AKiZ/sq0uv0mrtNdLqcGa0esY8kWn/w1Wn8NafT/AB5w6jNF8uWMndMUmY/DNOZ2/FvFrczvPp8k+neHtP03UYcuizZqdlfhzF5i/dXjjeeYjj0jaPXjmQbiuWlo3resxvMbxPvHqjj1GHL3fCy479luy3baJ2t8p/NqM3hrQZsFMV/iTWJ3tvbeMk/O0Tx688Qhbw1p8lovn1OoveOItWYpMxtEV32jmaxvET6/in5g3c58URaZyUiKztbe0cTx6/vH7p90fOHL4/BXTqV2nJmmZiKzb8MTMRWYmJnb0neOP9se0Jz4N6Z8WlqzlpjrvvirMRS287zvG235fnHHsDpJvWLxWbVi0xMxG/M//t4/dj5Oo6LHe9L6vBW9K91onJETWPnP7x+7RR4Q0m0RbV6u0VtSccTasdkVmZ24jnff3+UfJk6Xw3pcGG22S06j4kZa5YrWvZeNuYpEdsT+GN+OeQbT+Y6LumPN6feIidviV9J2iPf33j94ZM2iNt5jnhz2Twro8ndFs+oilo5rSa13n/unaOZ3iJifb9OGL/gzTzrMV76q9tPTHavZ2VibTb15iNort/piNt959QdVfJSkRN7VrEzERvO28ylu5rJ4S0ebVxlzZbXx1xY8VMfZWNop3bbztz/VxHpExE+qGDwfpadJ0+iyavUz8G03+JjmuOZtMRG+0R+UT+c8zvvIOo3hXlz4sNe7Llx0rtNt7WiOIjeZ/SIc9ovCOj0t881z57VzY8mK9bTXaa3249Pbb95lLH4S0GPLkvF8lq3pkx9tq0mIresxMRx+fHyiNvTgHRVvW1YtWYmsxvExPDyuSlrWrW1ZtXbuiJ5j9XOT4Q0drRF9Rnti7++2KYpNbTG+2/HO28/r77va+E9JXTZcMajUdt81c02/B3bxTt9duZ257p/FvtO/AOgtnxUyVpbLSL25rWbRvP6QnFomN4mHO6jwj07Niw457q1w0yUp21rvXu94nbjad5jZO3hjSzgtijPnrWdVGqiYmN6W22nadvfn9wb/AHh7u5mng7QV0c4IzaneazWbxeIm0bbRE8cxHrEem8z8256V0/D0zTfAwWtNO6bTNtptMzO8zM+//wABmhubgBubgBubgBubgBubgBubgBubgBuAwNfqNLgzY41Wpthtetpr+Oa12rG9pmfSNo95V21PT61mbdQiIiIn/mPaeY9/fadnvU+i6PqV4yailvjRERXJS8xasRO+0fLfeVFPDXTKZsmWuG8XyRFbz8W3MbTG3r6TvO/zBOut6dbBXNj11slLRWa/DyzeZi07RtEc8ylTVaK/we3U55+NSclJ7r7WiPX+/E8equfDfTPiZslMFseTLWaWvTJatuY2mYmJ4nbjdf8AyfT/ABO+LZ62+B5aO3LavbT/AG7f0z+cbTxHygGP/MenfDvedXqIrSk3nebxxE9s+3rEzETHtuurqdBPbvrrUta0VimTNNLd0+kTE8xP5PNP0PR6emWmP4sRkx0xz/mzxWkfhiPl+kcTvO/rKn/DHSezPXyv/Gr23nvtvMbbeu+8egMmubRXnauvi08ztGo34iImff2iY/dDHqun5KzanUImkW7O7zHG/bFtonfniYkx9A6dipqa4tP2RqYvGTttMbxad5/RRfwx0u2jxaaMFq48Xd8Oe+ZmvdERO2/6RP6wDJrqNBasWr1Cs1mKzExqeJi39Pv7+yM6rp0ZsmKepU+Lj/rp5nmvO3Mb8cvI8P8AT4/6d/WLb/FtvNomJ39fWZiN599nmp8O9M1MzOfSxebX75mbTzbfff8AeZ/eQXYcmjzWmuHXfEtFpptTUbz3R6x6+sMbT9R6Znpa9dfatax3T8TLan4donu5242mOfzX4eh6DBmplw4ZpesTEdt7RHMbem/y3/eXn8i6dtirXTVpTHWK1rSZrXj0naPWY3nn85Bj26p0us4o89ltOXFGakVte02pPMTER68ctlTBS9ItTNmmsxvExlnmGFl8PdOy5ceW+G3xMdIpW0ZLRO0R2+u/y3j9Jn5trWIrWIiIiI4iIBT5Wv1M33JPK1+pm+5K8BR5Wv1M33JPK1+pm+5K8BR5Wv1M33JPK1+pm+5K8BR5Wv1M33JPK1+pm+5K8BR5Wv1M33JPK1+pm+5K8BR5Wv1M33JPK1+pm+5K8BR5Wv1M33JPK1+pm+5K8BR5Wv1M33JPK1+pm+5K8BR5Wv1M33JPK1+pm+5K8BR5Wv1M33JPK1+pm+5K8BR5Wv1M33JPK1+pm+5K8BR5Wv1M33JPK1+pm+5K8BR5Wv1M33JSx4IpaLRfLP5WvMwtAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/Z'>

### Request information

*  Number of Resources: 1
*  Number of Hosts: 1
*  Number of JS Resources: 0
*  Number of CSS Resources: 0
*  Nubmer of Static Resources: 0
*  Total Request Bytes: 61

### Response data

*  HTML Response Bytes: 0
*  Text Response Bytes: 0
*  CSS Response Bytes: 0
*  Image Response Bytes: 0
*  JS Response Bytes: 0
*  Flash Response Bytes: 0
*  Other Response Bytes: 525

### Performance issues


### Mobile UX issues

*  Use legible font sizes
