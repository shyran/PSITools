---
layout: result
title: http://airecat.leandro.org/index.php?noinstall
MinimizeRenderBlockingResources: true
SizeTapTargetsAppropriately: true
UseLegibleFontSizes: true
---
## [L'aire a Catalunya](http://airecat.leandro.org/index.php?noinstall)

**Check against [PageSpeed Insights live](https://developers.google.com/speed/pagespeed/insights/?url=http://airecat.leandro.org/index.php?noinstall)**

**Score**: [74](https://developers.google.com/speed/pagespeed/insights/?url=http://airecat.leandro.org/index.php?noinstall)


<img src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAIUAUADASIAAhEBAxEB/8QAHAABAQEBAQEBAQEAAAAAAAAAAAECBgMHCAQF/8QANRABAAEBAwgKAgICAwEAAAAAAAERAgMFBAYhQVJVcZESExYYMVGTlNHSImEHsTKBCBShQv/EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAGBEBAAMBAAAAAAAAAAAAAAAAAAERIWH/2gAMAwEAAhEDEQA/APlCx4IsAQ0y0C6hIUBYQBVhAFahAFAAABaiFQUKgGs/YVBUKoBMgAAAJ4kgDM+KygH7RZQACQRJ8FSQRJVACDxAVaotQVUWAAjQAsCKC8QXxAhUAUKgAEaAP6AACQDSEgBUKgE6DzQBPNUmQQkJBOIGsBJVASqNTOhkCfBFlAAAIaZUFVmrQLGkSFAKgCqzCgtfFWVBVRAaEqoAAAAB5iVBSqAAhUAQmQJQAAASdYhIEpIkyAAAAAcQBVSPBQWCEUFJCQNR4ADSEALxVkBpIK6CoKABJpAAEqBrJEBRPMmQOCAASEgITIAys+KASgABqABIlQAAFQBqFZAaVlQUI/QAcABaiSAoi1BUKn+wAqagVEqAoh4AtUAAABJKgCTIgBIgAAAAJwVCJBSgABqAI/a6kWJBYWWTWDWoqkSoKJEkSC6AADiAGgACTUaz9gAAGgADUTrSoLxQAEn9EygGonQIAABSAAE/SzOhASJVnT/tYBVZWvMFAAAAiVQBVqlfMBVSCoLUqleagsCAKJ/QCiTJUFE0yAsylUKgohIFRAAlAAOIAAACSAipUE8Vhlagq1ZaBYEWtQI0AAEgAutAFiRPMBqolQFroImIQBqoyA1JVkBqZSonmClUKgGoqniC1ogAEgAawADzQCZ8gSZAmUAEIkAUSFBaqzraAVIAUCQA1AAAASAAQC1SoAtSqSAtUrIABrKAAABAABIAcEArUGZkFQJAqgABUAWEAVWVBaqyvgCrVlQURagAAAAAAAAAAAAAAGoAAqgLVJkQFJSoBrQQFqgAagAZWE4KCiVWkAAARKpB4Aq1QkF4CalqAtUgBa+Z/spACiHEFEKgokzJqBRKgKViGVpAFQNQAiAtUCQCqeMAAAAUgATxDiCESgCrCVUF1CeBEgoABrAFqvBkBRKrUFKpUBa6DQgDQyA1rT9oA0lYQBaiALKFf2VAEAJk1gAAAFUmQAAEnyEBNKwniQClRagKytfMFqRJ4AKJ4LWAOIGsAAD+wAFqhGgCvJUAWpVJANNAkAqVCoAAAeAAHgkyC8UqGsAKsyCzPNFqgBUmUAAAWqAKJXyWoLBEoA0M1WoKtUAWviMqCiFf0CiVUAKlQAqAHmJUFEqVBRKgFQABKlQVJlADzCqTIKgAAACLEgFPMAAAI/SoApKKAvggC1BAan9iFQVIKlQUSpWAWRKlQUSukqBrWiVNIKVZ8wFr5IABJUkBAADWAcQAA1IAhVQFZUFEqoAAAAAAFVQBRJAUQqDSJUqCiVWoKiVNPmCiHgC1KoAVAAAAAABJkFKpKAoICcFhAFWjKwCiKBH7XQgCkpUqC6gAAAKQAAcQA0AASajWABwACkAAagAgAAkmUmQXVpSvkAAIC0gQAmUAAAAABYQBYlWSAaEAUOKAtVqiA1UZ/tQUQqCiV5AKJUqCiaQFEQGq01pUAKggKHFJkFEqgLVABUAAADxVDzAWqKCKgB4KgCpIAtSulFAqaE8wGhAFI0JWSoKJUqCyJUroBRKlZ0gLVKgKVZAWtCsoAEgAaxQEPMBaoALqQAAUEAp5gAoIEAAEgBqABUADiACpAAAASAAoIGsoACggQAASAAAKgAcQAVABUAVABRAAUAQABQEAAVAVAAFQFQAUEABQTzFQAFBFlAAFAQABQDUgAAoIBQAFBAgABZBOAagAVAJ/8CkAAqcQA0KCBIACgh4C0gEBdQIEAALIIGoAFQAKAAqcQAAAAAAAADWACoAAAAAAAAAawAVAABQSQAFRQQ1gAqAAAAqAAAB4qCAtQQF1AgeCggqSBxFTWAKgAqAC6kjQB/QqABIACggLUEBdQIHgoISEgcQNYAqAf2KgAupADzABUAAAAAAAFQA8wAVAAAAAAABU8wAVAAAAAAABUAPMAAAAABUUEKeYoIqKCQAASpIJqABUVAOIAKkKgAAEioAoAmsoACp5gEAAEiyCAAqKgHEAFlF1ICoAAAKgAKgAAAqcAAAFQn/wAABUAAAFQAFQBUABUAAAFQAABRAAADgqABQAFQAg1AAsoawNQKAgAUioAKnEAFQAk1GsAU4IAtIQAXUgBAKCLKGsDUCgIAFIVAFTiAAAAHAAAAAAAAOIAAAAAAAH9AAAAACoAAAKgAHEkAOIAACof2AAAeKoeYC1RQRUAPBUACQBUFAQ8wFRUBdSeAAqABIAKCeYC1RQRdSAHgqABIAGsUBDzAVABdSAACggHEAFBAgAAkANQAKgASACpAAAASAAoIGsoACggQAASAAAKgASACoArz6672oTrrvbgHory6672oXrrvagG1efXXe1Cddd7UA9B59dd7UL113H/wBQDay8uuu9qDrrvbgHoa3n113tQvXXe1APRHn113tHXXe1APQY6672oOuu9qAeiPPrrvag6672oB6DHXXe1B113tQD0R59dd7cHXXe1APUefXXe1Cddd7cA9B59dd7UL113tQDYx113tQnXXe1APQefXXe1C9dd7cA2svLrrvag66724B6DHXXe1B113tQD0R59dd7R113tQD0GOuu9qDrrvagHpqR59dd7UHXXe1AOr/h7NHIc9c7rWF4plGU3GTWclvL/pZPNmLUzZmzERpiYp+Xk+293vNLemN+pdfR8l/4+5dc5Bn9bvcpv7q4sTkN9Zi1eW4sRMzNjRWeD9IdpcO3lkPubHyDie73mlvPG/Uuvod3vNLeeN+pdfR23aXDt5ZD7mx8naXDt5ZD7mx8g4nu95pb0xv1Lr6Hd7zS3njfqXX0dt2lw7eWQ+5sfJ2lw7eWQ+5sfIOJ7veaW88b9S6+h3e80t5436l19HbdpcO3lkPubHydpcO3lkPubHyDie73mlvTG/Uuvod3vNLeeN+pdfR23aXDt5ZD7mx8naXDt5ZD7mx8g4nu95pbzxv1Lr6Hd7zS3pjfqXX0dt2lw7eWQ+5sfJ2lw7eWQ+5sfIOJ7veaW9Mb9S6+h3e80t5436l19HbdpcO3lkPubHydpcO3lkPubHyDie73mlvPG/Uuvod3vNLemN+pdfR23aXDt5ZD7mx8naXDt5ZD7mx8g4nu95pbzxv1Lr6Hd7zS3njfqXX0dt2lw7eWQ+5sfJ2lw7eWQ+5sfIOJ7veaW88b9S6+jxyv+BsycjsWbeV43i1xYtWosWbV7f3NmJtT4RpseLvO0uHbyyH3Nj5fwYtieF4jYuotYvklzeXc2ujbsZRdTMRaszYtRSZmNNmZj9Cx1ylz/AeZl/XqcZxe8pETPQvrmaRNaeFj9Tyend7zS3njfqXX0dDm9lOB4BkX/UyDE8l6jx6NvKru1PS06a11xSP9cX+r2lw7eWQ+5sfITV44nu95pb0xv1Lr6Hd7zS3pjfqXX0dt2lw7eWQ+5sfJ2lw7eWQ+5sfIjie73mlvPG/Uuvod3vNLeeN+pdfR23aXDt5ZD7mx8naXDt5ZD7mx8g4nu95pb0xv1Lr6Hd7zS3njfqXX0dt2lw7eWQ+5sfJ2lw7eWQ+5sfIOJ7veaW88b9S6+h3e80t5436l19HbdpcO3lkPubHydpcO3lkPubHyDie73mlvTG/Uuvovd7zS3njfqXX0dr2lw7eWQ+5sfJ2lw7eWQ+5sfIOJ7veaW88b9S6+h3e80t6Y36l19HbdpcO3lkPubHydpcO3lkPubHyDie73mlvTG/Uuvod3vNLeeN+pdfR23aXDt5ZD7mx8naXDt5ZD7mx8g4nu95pbzxv1Lr6Hd7zS3pjfqXX0dt2lw7eWQ+5sfJ2lw7eWQ+5sfIOJ7veaW88b9S6+j5B/NmY2G5i41hmSYRlOV39zlWTWr61OUzZmYmLfR0dGI0P0r2lw7eWQ+5sfL4D/AMkMQyfEc4cFtZNlFzfxYyO3Fqbq8i3St5XTSQfIaRMUmIniRYs6Pws8oAE6FnZs8oOjZ0fhZ5AB0LOzZ5QvQs6fws8oAE6FnZs8oOhZrP42eQAdCzp/GzygmxZ2bPKAA6NnYs8oIsWdmzygAXoWdH4WeUJ0LOzZ5QAHRs6Pws8joWdmzygAXoWNizyhOhZ2LPKAA6NnYs8joWaz+FnlAATYs7FnlB0bOxZ5AB0LNP8AGzyXoWdH4WeUACdCzs2eUHRs6Pws8oADoWdmzyXoWdP4WeUACdCzs2eUHRs7FnkAL0LNf8bPKE6FnZs8oADo2dizyIsWaf42eQAdCzsWeUHQs7NnlAAdGzsWeUHQs7FnkAL0LGxZ5QREWfCIjhAA/9k='>

### Request information

*  Number of Resources: 14
*  Number of Hosts: 2
*  Number of JS Resources: 1
*  Number of CSS Resources: 7
*  Nubmer of Static Resources: 13
*  Total Request Bytes: 931

### Response data

*  HTML Response Bytes: 9472
*  Text Response Bytes: 0
*  CSS Response Bytes: 33840
*  Image Response Bytes: 16145
*  JS Response Bytes: 83955
*  Flash Response Bytes: 0
*  Other Response Bytes: 0

### Performance issues

*  Eliminate render-blocking JavaScript and CSS in above-the-fold content

### Mobile UX issues

*  Size tap targets appropriately
*  Use legible font sizes
