---
layout: result
title: http://fruitslots.justplaymobile.com/
MinimizeRenderBlockingResources: true
PrioritizeVisibleContent: true
---
## [Slot Machine](http://fruitslots.justplaymobile.com/)

**Check against [PageSpeed Insights live](https://developers.google.com/speed/pagespeed/insights/?url=http://fruitslots.justplaymobile.com/)**

**Score**: [78](https://developers.google.com/speed/pagespeed/insights/?url=http://fruitslots.justplaymobile.com/)


<img src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAIVAUADASIAAhEBAxEB/8QAHAABAAMBAAMBAAAAAAAAAAAAAAUGBwQCAwgB/8QANxABAAICAgEDAgMFBgYDAAAAAAECAwQFERIGITEHExQiQTJRYZGhFSNScYGCFjNCYrHBQ3Ky/8QAGgEBAAIDAQAAAAAAAAAAAAAAAAQFAQIDBv/EACoRAQACAgECBgEDBQAAAAAAAAABAgMRBAUhEhMxQVFhFAbw8RVxgbHB/9oADAMBAAIRAxEAPwD6pAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB6dzaw6Wrl2dm8Y8GKs3vaYmeoj5n2RF/VvCY8c3y70YqRETNsmO9I/rCX3MuHBq5cu1alMFKzbJa/7MV69+/4Mo9Wcrj1+DzauHz8M1+tTXnuclMfXxP6/wAYj5iPaf3RI42HzrxVF5fI/HpN+zR+N9ScLyexGvocpp7Ge3fWPHlibT189R8pZ89/Sacn/GmhExP2vu3me/0tGHJ/6mX0JDfm8aONk8uJ259O5k8zD5sxruA8K5KWtatb1tavzET3MIic8w7/AMzsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGZ/Wrm8mhp8bpYbxH38lsuSP3xTrxif4eUxP+2Gb+leuV5/Vpv5Yy4trPjwXpXJ3e1Zt3aIiPeInxiJn29pn976G3+J47kM2PLv6Ors5McTFLZsVbzWJ+eu3v1tTX1a+OtgxYa/ux0iv/hY4edXFgnFWvefdVZ+m2z8mM1r9o9tKTxfo7Nwnq3VzcZTFPDTmy7Nu7dZMF5xTSKf91e57j93vH7l42M+PWwXzZ7xTFSPK1pn2iHsVb6kb8cf6djJ+1a2fHWtP8U999f0Q5tfNaN95T8eOmCs+HtHqg/XvrnHocZkiuvuY8VaTky2msR5V69qdxPceU9R/DtXPSP0w4zlvTmtyXP1zRye75bOW2K0U/bnuP07/AMv4dITncGXn/UXAen7za07Hhu79pjqYx19+v8pmJ6/+sNly7NMWKKY4itKx1ER+kfpCNzOfPAp4InV/fXx7N8fHjlWi3rX238/uGb7/ANLOJ14mdTleZw9fEV2p6j+XSs8Bk3vSv1D47Fo7vI7+pGzi1NmM+xe1fHLS0zHU2mO48YtHt+ktP5Pc8u4iWX8Dh/H+tNPc8rdbG1tbEdz1EVxUjHSf55PlX9D63l53NvizX3Xwz21Hr/ES7c7psYcNcmGupiY3O/b+dPo2J7h+on0xtW2+IxWy3tky0tbHa1vmZifaZ/06Sy1tHhnUucTuNgDDIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD8memfeqsmHl4rtbWWK8dg2JxVr3/01racl7de8fHUfwif3tBtEWiYmO4n5Zh9ZdHWro8VhrP4f8fu11suaJ8esc1t5Vmf+6O6R3+tnTHetN2n2aWpN9Vj3Z/6S9S6elznJc9ytM9Lch1XVtXH5xjwR7VrPXvE+MV/T9ZXOvrLiN2OsHI4PKf+m9vCf5W6UL1Hxt75/u6eGM2tjjwm2GO4ifaevy/yQFuNvknxjDmrM/pMRaP69f8Alww/pavWuJHLz5Zpe+57amIjfbtP19oXUf1H/S+bbi4KRalNR33E7139Pv6ajy+/FON2tqt4tXHitaJrPcd9e39enL9OtGn/ABFx+LJETGtxVbXif8WbLe3/AOcdVU4H09GXBWsRWb3t114zXv39vjuF24bYjjOb9RbOK2Pxx5sWnScncREYsFImPbue/K1nneh9N/C5XIxYbeOa7jfpufT5n7em5/InJwsWXLHh8ep166jW/iPmFs+n18mrznO8XTJ56eDJGSkW+azaZjqJ/d1Ve2ffTrDt7G3k5S2GcePYrb7lrVmvl8ePjE+8/Hz/ABWP1Vz+Tgf7PtTRyblNnNbHeMV4i9YjHa8zWJ/anqs+3cPWZK2tfXv23/fXdRY71pj8U+n/ABOZL1x0m+S0VpWO5taeohD4fVPCZ+Vxcbr8nrZt3L3FceK3n8R3Pcx7R7RPzLLfqry2lzP4Ha4vbruYcmn5TimZtXH/AHtYi3hPtF57vHvHf5f4OT6U8Hs19X6Gzek1x4ceTNb26iIms0j+c2/pKVTg18ic17a17faBk6jb8mvHx13vXf6buECuWwAAAAAAAAAAAAAAAAAAAAAAAAAAAAgfVnp7D6j066m31Ov1aL1mInyiY6/0n9YmPeJjuE8M1tNZ3DExvsxDm/onu02fxvpr1JtYNmvxXambd/w+5Xq0/wC7ycmbD6j4PHgr6m9PZtnDjnrLu6s/ia2rFZ948I86zNvH5rMREy3oTMXOyY6xTtqP8f6RM/BxZ7eO3qzb6d5/TnL4sX4De1tjc1p8LYa5PzR179+ExFv1/d89on0lh3OV4rbz6eHJb+0d/Z2LZKx7RW2a0R79/wCGIn/RoPP+keB5/q3LcVq58sfs5vDxy1/yvXq0fzdfp7hdPgONxaHG0mmriiIpWZ78Yj4jtEwUw8etvLjvM/vul5rZM8x457Q78GOuHDTHT2rSsVj/AChT/qxXLHpaNjB393X2cdq/7u8c/wBLyujj5fjsHK8dm0trzjFliO5x2mtomJiYmJ/SYmIltiv4LxafZzz4/Nx2x/MafPH0/wBSm/zGDBsWmdeJjJNP0mY/Wf3y270n9nWtmwbFYxcpk/Pkr11W1Y9q/bn9aRH+vvMzETLm0Pp56e0s05sWvsWyzbym1trJ/wCphN6PA8bo7NdjW1KVz1rNYyWmbWiJ+feZmU3ncyvJtuu4hXdN4F+JXV9TKTAVy2AAAAAAAAAAAAAAAAAAAAAAAAAAAV/1h6u4n0hqYNnm8ufHizXtSv2de+aY8aWva0xSJmK1rW1pn4iIBYBTN76mel9PLy2K+7myX4zFbNsRh1cuSPCs1i80mK9X8JvXy8e/Hv36dGP1/wADk+9SmbanYw8bXlr68amT70a9pmInw678vb9nrv3j29wWsVDhvqHwPMb/ABOno2375+T1vxeCLaOasRh7tEXvM16pEzS3U2679uvmHPs/VH0pr6+zlyb+afw2WMOWlNTLe9cneSJp4xWZ8ojDktMde1Y8p9pjsLuKdufUj0zq4817b18tcfl/ycF8nn1GCfy+Mfm7jZw9dfPl7fEvPN9RPTeHT4PaybmWMHMXnHrW/DZPa1bxS33I6/u+r2rWZt11M9AtwpOx9UPSmDQ/F35DJbHNMWSK49bJkvaMkZJr1StZmeow5JmOvyxXuenns/U30pr/AIzy5SLfhMd82X7eK9+qVxY8s2jqPeJplpMTHz3MR8SC5ji4XksHMcXr7+rXPTBnr50rnw2xXiO+veloiY+P1h2gAAAAAAAAAAAAAAAAAAAAAAAAAAAAKn9Q/QnGeu9DV1OXy7OPHrZLZsc4LxWYvOO1In3ie+vLvr4nrqe47hbCfgFBv9MuMybXNZb8hyNsXI623r115vT7ep+K6+/fFHj3E2msT7zMRPfUREvfg+nPG4fVuf1DTe5H8dmwTrXpOWPtzinDTHFfHrqOvCLd/Pffft7KR6g2vWfF6/O5MfLeocmr/bdtembBx+PYzYNeNb7mOcWOMf5q2zXrjm3U+1Y+PeXfzG59Qtj1HwMaNdjXx04bDtcljrXH9iM8+f3ae9Zta/tWKxWY6nqZ7iQWXQ+nGjoeo/TvMa2/txl4bjKcVTHamK0ZsVK2iJtM18qzPl7+Mx31D95f6c8fv03rYOQ5Dj9vZ5K3JxtalqVyYsl8EYL1r3WYms0iY94me7d9/CncV6x9UcpzPorHg1earxmXj64OY2bcfOPx28lZrFpi9YmPC+PvuImv5/f26d85PWmT6O83vbXJ71PUmO2a2v8AhtWtbxGG326xFJrPcZPtzefb/wCT26joEhs/R30/nybdJ2uQro58FMFdOMtft4oj7EWtWevLu1dXFWe5/SeuplJ4fprwddf0vg2fv7eD079z8HjzTHVptatqzfqI8pr4V67+Z957n3Z/6j5r6icdu+otTSnkM+jp6+5XT3PsRfLmvTX1ppM1jH1+1bJMWjvytNo6/L07fU/qL1Xpeg/SO5xVuc2MmWl77l8WnF9nJlrETTFaLYY8azPn3acde4pWPby7BNa30Y4LR1ZpxW/yXHbX93P4vVtjrl861zUvee6zE2vTPatpmP0rMdTBs/RX01kyb98GXc177enl0bTjtWfHFbFixxX3ie/GMMTHf+K3fft1W+U9VfUDLyHqOOK47koieU1J4mmXViuPJgrm+1mpNuu4rbxi0zbqYi8zHt7vZ9Luc9a7frbjdT1fm5SmKeMte2OdXww3zxlyxbymMXXtWK9T51+I9rdg130vxEcD6f0eKpsX2KamKMVcl6UpM1j4/LSIrHUdR7RHwlAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6AAOgA6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//Z'>

### Request information

*  Number of Resources: 48
*  Number of Hosts: 1
*  Number of JS Resources: 1
*  Number of CSS Resources: 0
*  Nubmer of Static Resources: 47
*  Total Request Bytes: 3561

### Response data

*  HTML Response Bytes: 4074
*  Text Response Bytes: 0
*  CSS Response Bytes: 0
*  Image Response Bytes: 1467887
*  JS Response Bytes: 78860
*  Flash Response Bytes: 0
*  Other Response Bytes: 0

### Performance issues

*  Eliminate render-blocking JavaScript and CSS in above-the-fold content
*  Prioritize visible content

### Mobile UX issues

